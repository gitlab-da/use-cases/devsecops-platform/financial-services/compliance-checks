import numpy as np
# General imports

import pandas as pd
import streamlit as st
import streamlit.components.v1 as components
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from deepchecks.tabular import Dataset
from deepchecks.tabular.suites import full_suite
from deepchecks.tabular.datasets.classification import iris




# Load Data

iris_df = iris.load_data(data_format='Dataframe', as_train_test=False)
label_col = 'target'
df_train, df_test = train_test_split(iris_df, stratify=iris_df[label_col], random_state=0)

# Train Model
rf_clf = RandomForestClassifier(random_state=0)
rf_clf.fit(df_train.drop(label_col, axis=1), df_train[label_col]);




# We explicitly state that this dataset has no categorical features, otherwise they will be automatically inferred
# If the dataset has categorical features, the best practice is to pass a list with their names

ds_train = Dataset(df_train, label=label_col, cat_features=[])
ds_test =  Dataset(df_test,  label=label_col, cat_features=[])




suite = full_suite()
a = suite.run(train_dataset=ds_train, test_dataset=ds_test, model=rf_clf)
a.save_as_html('my_results.html')

html_file_path = "my_results.html"


with open(html_file_path, 'r') as html_file:
    html_code = html_file.read()

# Embed the HTML code in the Streamlit app
components.html(html_code, height=1000, width=1000)
#components.iframe("https://account-l6bqixsvvq-uc.a.run.app/", width=1085, height=700)


st.write(a)