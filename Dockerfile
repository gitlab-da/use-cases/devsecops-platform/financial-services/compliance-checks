FROM python:3.10-slim-bullseye
LABEL maintainer="William Arias"
COPY .  /app/ 
WORKDIR /app
EXPOSE 8501
RUN pip install -r requirements.txt 
ENTRYPOINT ["streamlit", "run"]
CMD ["src/deep-check.py"]
